/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2022 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

import sourcemaps from 'rollup-plugin-sourcemaps';

export default {
  input: 'lib/index.js',
  plugins: [sourcemaps()],
  output: {
    file: 'index.js',
    format: 'umd',
    name: 'Main',
    sourcemap: true,
    globals: {
      '@preserves/core': 'Preserves',
      '@syndicate-lang/core': 'Syndicate',
      '@syndicate-lang/html': 'SyndicateHtml',
      '@syndicate-lang/ws-relay': 'SyndicateWsRelay',
    },
  },
};
